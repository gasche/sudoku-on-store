(* Parsing the command-line *)
type strategy =
  | MinBranch
  | Chains_MinBranch
  | Chains_Score
  | Chains_MinBranch_Score

let rank2 = ref 9
let only = ref 0
let digits = ref ""
let strat = ref Chains_MinBranch_Score
let magic = ref false
let bench = ref false
let quiet = ref false
let count = ref false
let timer = ref false
let minimize = ref false
(* let gen = ref false *)
let logic = ref false
let range = ref ""

let arg_spec = [
  "-rank", Arg.Set_int rank2, "the rank of the grid 1..30 (default:9)";
  "-only", Arg.Set_int only, "stops after n solutions";
  "-digits", Arg.Set_string digits, "the string of digits";
  "-magic", Arg.Set magic, "magic square (no box constraint)";
  "-strat", Arg.Int (function
		       | 1 -> strat := MinBranch
		       | 2 -> strat := Chains_MinBranch
		       | 3 -> strat := Chains_Score
		       | 4 -> strat := Chains_MinBranch_Score
		       | _ -> assert false),
  "strategy 1,2,3,4";

  "-bench", Arg.Set bench, "bench mode";
  "-range", Arg.Set_string range, "range (for the bench mode)";
  "-quiet", Arg.Set quiet, "don't print the solution";
  "-count", Arg.Set count, "print the number of solutions";
  "-timer", Arg.Set timer, "display computation time";
  "-minimize", Arg.Set minimize, "minimize mode";
(*  "-gen", Arg.Set gen, "gen mode"; *)
  "-logic", Arg.Set logic, "logic mode";
]

let usage_msg = "Sudoku solver\n"

let usage _ = Arg.usage arg_spec usage_msg; exit 1

let () = 
  Arg.parse arg_spec usage usage_msg;
  if !magic then
    (if (!rank2 <= 1) || (!rank2 >= 30) then
       (Printf.eprintf "Error: rank must be in the range 1..30\n"; exit 2))
  else 
    (match !rank2 with
       | 9 | 15 | 16 | 25 -> ()
       | _ ->
	   Printf.eprintf 
	     "Error: rank must be 9,15,16,25 for sudokus\n"; exit 2);
  if !only < 0 then
    (Printf.eprintf "Error: only myst be >= 0\n"; exit 2);
  if !digits <> "" && String.length !digits != !rank2 then
    (Printf.eprintf 
       "Error: wrong digit string (length=%i)\n" 
       (String.length !digits); exit 2)

    

let rank2 = !rank2
let rank4 = rank2 * rank2

let strat = !strat

(* Global backtracking state *)

(* module Store = StoreBacktrackingRef *)
module Store = StoreCustom

let store = Store.create ()
module SRef = Store.Ref

module SArray = struct

  module Custom = struct
    type 'a field = int
    type 'a op = Set of 'a [@@unboxed]
    type 'a structure = ('a op, 'a field) Store.structure

    let structure (data : 'a array) (gens : Store.Generation.t array) : 'a structure =
      let apply idx (Set v) g =
        data.(idx) <- v;
        gens.(idx) <- g;
      in
      let antiop idx (Set _) =
        Set data.(idx), gens.(idx)
      in
      let lower_generation idx g =
        gens.(idx) <- g
      in
      { apply; antiop; lower_generation; }
  end

  type 'a t = {
    data : 'a array;
    gens : Store.Generation.t array;
    structure : 'a Custom.structure;
  }

  let make n v =
    let data = Array.make n v in
    let gens = Array.make n Store.Generation.zero in
    let structure = Custom.structure data gens in
    { data; gens; structure; }

  let[@inline] get _s t idx =
    t.data.(idx)

  let set (s : Store.t) t idx v =
    let gstore = s.Store.generation in
    let gfield = t.gens.(idx) in
    if gstore = gfield then
      (* record elision *)
      t.data.(idx) <- v
    else begin
      let structure = t.structure in
      let antiop = Custom.Set t.data.(idx) in
      s.root <- Store.structure_log s.root structure idx antiop gfield;
      t.data.(idx) <- v;
      t.gens.(idx) <- gstore;
      ()
    end
end

let sref_array_make n v = SArray.make n v
let[@inline] ( .%() ) a i = SArray.get store a i
let[@inline] ( .%()<- ) a i v = SArray.set store a i v

(* Global constants *)

let chars = 
  if !digits <> "" then !digits
  else String.sub "123456789ABCDEFGHIJKLMNOPQRSTU" 0 rank2  

(* Matrix.

   A grid is represented as a one dimensional integer array of size rank4+1.
   The cell 0..rank4-1 are bit fields which indicate the possible
   digits for the corresponding cell. The last cell of the grid
   store the number of resolved cells. *)

let coord x y = x + rank2 * y

let blocks = Array.concat [
  Array.init rank2 
    (fun y -> Array.init rank2 (fun x -> coord x y)); (* lines *)
  Array.init rank2 
    (fun x -> Array.init rank2 (fun y -> coord x y)); (* columns *)
  if !magic then [||]
  else 
    let w,h =
      match rank2 with
	| 9 -> 3,3
	| 15 -> 5,3
	| 16 -> 4,4
	| 25 -> 5,5
	| _ -> assert false
    in
    assert (rank2 == w * h);
    Array.init rank2 
      (fun i ->
	 let x0 = (i mod h) * w
	 and y0 = (i / h) * h in
	 Array.init rank2 (fun j -> coord (x0 + (j mod w)) (y0 + (j / w)))
      )  (* boxes *)
]

let regions =
  let r = Array.make rank4 [] in
  for i = 0 to Array.length blocks - 1 do
    for j = 0 to Array.length blocks.(i) - 1 do
      r.(blocks.(i).(j)) <- i :: r.(blocks.(i).(j))
    done;
  done;
  Array.map Array.of_list r


let compute_neighbours i =
  let res = ref [] in
  for j = 0 to Array.length regions.(i) - 1 do
    for k = 0 to Array.length blocks.(regions.(i).(j)) -1 do
      let z = blocks.(regions.(i).(j)).(k) in
      if (z <> i) && (not (List.mem z !res)) then res := z :: !res
    done;
  done;
  Array.of_list !res

let neighbours = Array.init rank4 compute_neighbours


(* Bit-field manipulation *)

(* rank of the least significant 1 bit *)
let bit_rank k =
  let rec aux i k = if (k = 1) then i else aux (succ i) (k lsr 1) in
  aux 0 k

(* keeps only the least significant 1 bit *)
let least_bit n = n land (lnot (n - 1))

(* is 0 or 1 bit set to 1 ? *)
let only_one_1 n = (n == least_bit n)

(* number of 1 bits *)

let count_bits n =
  let rec l i n =if n = 0 then i else l (succ i) (n land lnot (least_bit n)) in
  l 0 n

let nb_bits_byte = Array.init 256 count_bits

let nb_bits n =
  nb_bits_byte.(n land 0xff) +
  nb_bits_byte.((n lsr 8) land 0xff) +
  nb_bits_byte.((n lsr 16) land 0xff) +
  nb_bits_byte.((n lsr 24) land 0xff)


let check_count a =
  let n = ref 0 in
  for i = 0 to rank4 - 1 do if only_one_1 a.%(i) then incr n done;
  if !n != a.%(rank4) then (
    Printf.eprintf "n=%i a.(rank4)=%i\n" !n a.%(rank4);
    assert false
  )

(* Prints a grid *)

let print a =
  for y = 0 to rank2 - 1 do
    for x = 0 to rank2 - 1 do
      let i = coord x y in
      if a.%(i) != 0 && only_one_1 a.%(i) then print_char chars.[bit_rank a.%(i)]
      else print_char '.'
    done;
    print_newline ();
  done

let dump a =
  for y = 0 to rank2 - 1 do
    for x = 0 to rank2 - 1 do
      let i = coord x y in
      Printf.printf "%08x," a.%(i);
    done;
    print_newline ();
  done

exception Finished

let counter = ref 0

let solution a =
  incr counter;
  if not !quiet then (print a; print_endline "---");
  if (!only > 0) && (!counter = !only) then raise Finished


let real_blocks = SRef.make store blocks
let real_neighbours = SRef.make store neighbours

let filter_block a block =
  let res = ref [] in
  let f = ref false in
  for i = 0 to Array.length block - 1 do
    if not (only_one_1 (a.%(block.(i)))) then res := block.(i) :: !res
    else f := true;
  done;
  if !f then Array.of_list !res else block

exception Impossible

let rec propagate changes a i =
  a.%(rank4) <- a.%(rank4) + 1;
  let n = (SRef.get store real_neighbours).(i) in
  let m = lnot a.%(i) in
  for k = 0 to Array.length n - 1 do
    let i' = n.(k) in
    let old = a.%(i') in
    let n = old land m in
    if n != old then restrict changes a i' n
  done

and restrict changes a i n = 
(*  assert(a.%(i) != n); *)
  if n = 0 then raise Impossible; 
  a.%(i) <- n;
  let c = regions.(i) in
  for j = 0 to Array.length c - 1 do changes.(c.(j)) <- true done;
  if only_one_1 n then propagate changes a i;

(* A block is a group of rank2 cells (a lines, a column, a box) such
   that each digit must appear exactly once in this group of
   cells. The function sets digits according to this rule and returns
   the coordinates of the modified cells. *)
    
and find_in_block changes a block =
  let gte1 = ref 0    (* unresolved digits that appears at least once *)
  and gte2 = ref 0 in (* unresolved digits that appears at least twice *)
  let l = Array.length block in
  for i = 0 to l - 1 do
    let n = a.%(block.(i)) in
(*    if not (only_one_1 n) then *) (
      gte2:= !gte2 lor (!gte1 land n);
      gte1:= !gte1 lor n
    )
  done;
  gte1 := !gte1 land lnot !gte2; (* exactly once *)
  if !gte1 !=0 then (
    let i = ref 0 in
    while (!i < l) do
      let j = block.(!i) in
      let n = a.%(j) land !gte1 in
      if n != 0 then (
	if not (only_one_1 n) then raise Impossible;
	if a.%(j) != n then restrict changes a j n;
	gte1 := !gte1 land lnot n;
	if (!gte1 == 0) then i := l else incr i;
      ) else incr i;
    done;
    true
  ) else false

let saturate changes a =
  let b = SRef.get store real_blocks in
  let f = ref true in
  while !f do
    f := false;
    for i = 0 to Array.length changes - 1 do
      if changes.(i) then (
	changes.(i) <- false;
	if find_in_block changes a b.(i) then f := true
      )
    done;
  done


let restrict a i n = 
  let changes = Array.make (Array.length blocks) false in
  restrict changes a i n; saturate changes a

let may_restrict a i n =
  if a.%(i) != n then restrict a i n

(* Chains.

   For each unresolved cell, try all the possible choices one by one
   and deduce all the consequences from each one.

   - If we get a contradiction, we can reduce the number of possible
   choices.

   - For all the choices which does not imply an immediate contradiction,
   we consider the set of remaining choices for all cells, and we
   take the unions of all these choices. This set might be smaller
   than the original set of choices.

   - We also compute a best branching cell. 

   - We iterate as long as something changed.
*)

let or_array a1 a2 =
  for i = 0 to Array.length a1 - 1 do a1.(i) <- a1.(i) lor a2.%(i) done

let alpha = 1. /. 8.

let rec chains a =
(*  Printf.eprintf "Chaining r=%i deep=%i\n" a.%(rank4); flush stderr;  *)
  let r0 = a.%(rank4) in
  let possible = Array.make rank4 0 in
  let change = ref false in
  let min_cost = ref infinity in
  let _best1 = ref (-1) in
  let best2 = ref (-1) in
  let min_branch = ref 1000 in
  for i = 0 to rank4 - 1 do 
    if only_one_1 a.%(i) then ()
    else (
      for z = 0 to rank4 - 1 do possible.(z) <- 0 done;
      let cost = ref 0. in
      for k = 0 to rank2 - 1 do
	let m = 1 lsl k in
	if (a.%(i) land m <> 0) then
	  try
            Store.temporarily store (fun () ->
              may_restrict a i m;
	      or_array possible a;
	      if not !change then
	        cost := !cost +. (alpha ** (float_of_int (a.%(rank4) - r0)));
            )
	  with Impossible ->
	    let n = a.%(i) land lnot m in
	    restrict a i n;
 	    change := true
      done;
      (match strat with
	 | Chains_MinBranch ->
	     let nbb = nb_bits a.%(i) in
	     if nbb < !min_branch then (min_branch := nbb; best2 := i)
	 | Chains_Score ->
	     if !cost < !min_cost then (min_cost := !cost; best2 := i)
	 | Chains_MinBranch_Score ->
	     let nbb = nb_bits a.%(i) in
	     if nbb < !min_branch 
	     then (min_branch := nbb; best2 := i; min_cost := !cost)
	     else if nbb = !min_branch && !cost < !min_cost 
	     then (min_cost := !cost; best2 := i)
	 | _ -> assert false
      );

      for z = 0 to rank4 - 1 do
	let o = a.%(z) in
	let n = possible.(z) land o in
	if n != o then (restrict a z n; change := true);
      done;  
    )
  done;
  if !change then chains a else !best2


(* Find a cell whose number of remaining choices is minimal and >= 2 *)

let least_var a =
  let cur_min = ref max_int and cur = ref (-1) in
  (try for i = 0 to rank4 - 1 do
     let n = nb_bits a.%(i) in
     if n = 0 then raise Impossible;
     if (n = 2) then (cur := i; cur_min := 2; raise Exit);
     if (n > 1) && (n < !cur_min) then (cur_min := n; cur := i)
   done with Exit -> ());
  !cur

let rec main_loop2 a =
  if a.%(rank4) == rank4 then solution a else
  let i = if strat = MinBranch then least_var a else chains a in
  if i < 0 then solution a
  else for k = 0 to rank2 - 1 do
    let m = 1 lsl k in
    if a.%(i) land m <> 0 then
      try
        let step () =
	  may_restrict a i m;
          main_loop a;
        in
        if a.%(i) land lnot (m lsl 1 - 1) = 0 then
          step ()
        else
          Store.temporarily store step
      with Impossible -> ()
  done

and main_loop a =
  let old_bs = SRef.get store real_blocks in
  let old_n = SRef.get store real_neighbours in
  Store.temporarily store (fun () ->
    SRef.set store real_blocks (Array.map (filter_block a) old_bs);
    SRef.set store real_neighbours (Array.map (filter_block a) old_n);
    main_loop2 a
  )

let all_bits = ((1 lsl rank2) - 1)

let solve_grid clues = 
  let a = sref_array_make (rank4 + 1) all_bits in
  a.%(rank4) <- 0;
  counter := 0;
  (try
     List.iter (fun (i,m) -> may_restrict a i m) clues;
     main_loop a
   with Impossible | Finished -> ())

let logic_solve_grid clues = 
  let a = sref_array_make (rank4 + 1) all_bits in
  a.%(rank4) <- 0;
  counter := 0;
  (try List.iter (fun (i,m) -> may_restrict a i m) clues; 
     ignore (chains a); 
     solution a
   with Impossible | Finished -> ())

let time_stamp () =
  let tm = Unix.localtime (Unix.time ()) in
  Printf.printf "[%02i:%02i:%02i] " 
    tm.Unix.tm_hour tm.Unix.tm_min tm.Unix.tm_sec


let minimize_clues clues =
  only := 1;
  let real_quiet = false in
  quiet := true;

  let a = sref_array_make (rank4 + 1) all_bits in
  List.iter (fun (i,m) -> a.%(i) <- m) clues;
  time_stamp ();
  Printf.printf "Starting to minimize:\n";
  print a;

  let clues = 
    List.sort
      (fun _ _ -> if Random.int 2 = 0 then 1 else -1) clues
  in
  let rec aux depth before = function
    | ((i,m) as hd)::after ->
      (
	time_stamp ();
	Printf.printf "Cell %i (%i remaining, %i removed, %i total) ..."
	  i (List.length after) depth (List.length after + List.length before);
	flush stdout;
	solve_grid ((i, all_bits land lnot m) :: before @ after);
	if !counter = 0 then (
	  if not real_quiet then (
	    Printf.printf " removed\n";
	    flush stdout;
	    let a = sref_array_make (rank4 + 1) 0 in
	    List.iter (fun (i,m) -> a.%(i) <- m) (before @ after);
	    print a;
	    flush stdout;
	  );
	  aux (succ depth) before after
	)
	else (
	  if not real_quiet then (
	    Printf.printf " kept\n";
	    flush stdout;
	  );
	  aux depth (hd::before) after
	)
	)
    | [] ->
	let a = sref_array_make (rank4 + 1) 0 in
	time_stamp ();
	List.iter (fun (i,m) -> a.%(i) <- m) before;
	print_endline "MINIMAL PUZZLE:";
	print a
  in
  aux 0 [] clues

let read_clues () =
  let clues = ref [] in
  let rec loop i = if i == rank4 then () else match input_char stdin with
    | '0' | '.' -> loop (succ i)
    | '\n' | '\r' ->
	if (i mod rank2 != 0) then
	  (Printf.eprintf "End-of-line on the middle of a line\n"; exit 2);
	loop i
    | c ->
	let n = try String.index chars c with Not_found -> -1 in
	if n >= 0 then (clues := (i,1 lsl n) :: !clues; loop (succ i))
	else loop i
  in
  (try  loop 0;
   with End_of_file when not !bench ->
     Printf.eprintf "End-of-line before the end\n"; exit 2);
  !clues

let one_puzzle prefix =
  let clues = List.rev (read_clues ()) in
  let t0 = Unix.gettimeofday () in
  if !minimize then minimize_clues clues
  else 
    if !logic then logic_solve_grid clues
    else solve_grid clues;
  let t = Unix.gettimeofday () -. t0 in
  if !count then Printf.printf "%s solutions: %i\n" prefix !counter;
  if !timer then Printf.printf "%s time: %6i ms\n" prefix (int_of_float (t*.1000.));
  flush stdout

let parse_range range =
  try 
    let i = String.index range '-' in
    int_of_string (String.sub range 0 i),
    int_of_string (String.sub range (succ i) (String.length range - i - 1))
  with Not_found | Failure _ | Invalid_argument _ ->
    Printf.eprintf "Invalid range specification (must of the form 10-15)\n";
    exit 2
  

let () =
  if !bench then 
    try
      let (min,max) = 
	match !range with "" ->(1,max_int) | _->parse_range !range in
      let skip = ref (min - 1) in
      let no = ref 1 in
      while !no <= max do 
	if (!skip > 0) then (
	  decr skip;
	  ignore (read_clues ())
	) else 
	  one_puzzle (Printf.sprintf "Puzzle %2i: " !no);
	incr no;
	match input_char stdin with '\n' | '\r' -> ()
	  | _ -> Printf.eprintf 
	      "Puzzles must be separated with a newline character\n";
	      exit 2
      done
    with End_of_file -> ()
  else one_puzzle ""
