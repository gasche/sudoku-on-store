all: sudoku

#sudoku_c: sudoku_c.c topo.c
#	gcc -o sudoku_c sudoku_c.c -Wall -lm -O2

#topo.c: sudoku
#	./sudoku -rank 25 -gen > topo.c

sudoku: Store.ml StoreCustom.ml StoreBacktrackingRef.ml sudoku.ml
	ocamlopt -o sudoku -unsafe -I +unix unix.cmxa Store.ml StoreCustom.ml StoreBacktrackingRef.ml sudoku.ml

#bench: bench.ml
#	ocamlopt -o bench unix.cmxa bench.ml

clean:
	rm -f *~ *.cm* *.o bench sudoku sudoku_c topo.c

bench: sudoku
	./sudoku -rank 25 < puzzles/bench25

PACKAGE_FILES = Makefile README puzzles sudoku.ml
DATE=`date +%Y%m%d`

package:
	rm -Rf sudoku-$(DATE)
	mkdir sudoku-$(DATE)
	cp -aR $(PACKAGE_FILES) sudoku-$(DATE)
	tar czf sudoku-$(DATE).tar.gz  --exclude CVS --exclude ".#*" --exclude "*~" --exclude "#*#" sudoku-$(DATE)
	rm -Rf sudoku-$(DATE)
