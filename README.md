This small project is a port of a Sudoku solver by Alain Frisch (<http://alain.frisch.fr/sudoku.html>, 2005) to use the [Store](https://gitlab.com/basile.clement/store) library. We use this as a macro-benchmark for Store and to get usage feedback on its API design.


## Current versions

As branches:

- `base-version`: Alain Frisch's original code.
- `Store-version`: a version using Store references
- `ref-version`: a version using plain references,
   with the exact same behavior as `base-version` but
   a memory layout similar to `Store-version`.
- `StoreBacktrackingRef-version`: uses the semi-persistent
  StoreBacktrackingRef implementation of Store (JFLA'23)
- `StoreCustom-version`: uses the "custom locations" support
   of the `jfla` branch to implement versioned arrays with
   a better memory layout.
- `StorePersistent-version`: like Store, but uses the persistent API
  to implement `temporarily`
- `StoreVector-version`: uses StoreVector from union-find
  (this corresponds to using a "generic" full-copy implementation, instead of the "hand-crafted" full-copy version in Alain's code)

## Current results

Command: `make && time make bench`.

On Gabriel's machine:
- `base`: 1.35s
- `ref`: 2.00s
- `Store`: 1.63s
- `StoreBacktrackingRef`: 1.65s
- `StoreCustom`: 1.74s
- `StorePersistent`: 1.76s
- `StoreVector`: 4.03s

| Implementation    |  Time | Relative |
|:------------------|------:|---------:|
| `base`            | 1.35s |     1.00 |
| `Store`           | 1.63s |     1.20 |
| `StorePersistent` | 1.76s |     1.30 |
| `StoreVector`     | 4.03s |     2.99 |

## Current analysis

Moving from `int array` to `int ref array` for the Sudoku (an extra indirection, which gets copied at each backtracking point) has a large performance impact, adding about 0.7s of compute time.

Moving from `int ref array` to `int Store.Ref.t array`, which requires less copies at backtracking points, improves performance by about 0.4s. As a result, the direct `Store` version is only 20% slower than the original code.

`StoreBacktrackingRef` (from JFLA'23) has the same implementation as Basile's Store on this test.

My attempt in `StoreCustom` to implement "custom arrays" (instead of arrays-of-references) to get more efficient has failed. In fact the result is slightly slower than the direct `Store` version.

My hypothesis: the `ref` layout is *more costly* with the non-Store version than with Store versions, because the `base` version performs shallow copies of the array at backtracking point, and those copies are sensibly slower when they must also do a shallow copy of each reference. It may have been wrong to infer from the 1.3->2.0s difference that unboxing the layout of the Store version would improve performance noticeably.
